import 'dotenv/config';
import { createClient } from 'redis';
import {
    calculateValue,
    getByFindex,
    handleError,
    storeTicketFnumber,
    storeFindexFnumber,
} from './functions';

let redisClient: any;

const postTicketProcessing = async (messageObject: any): Promise<void> => {
    const bodyObject = JSON.parse(messageObject.body);
    const fIndex = parseInt(bodyObject.number, 10);
    if (fIndex < 1) {
        await handleError('Number should be > 0');
        return;
    }

    let fibonacci: string = await getByFindex(redisClient, fIndex.toString());
    const ticketId = messageObject.ticketId;
    if (fibonacci === '') {
        fibonacci = calculateValue(fIndex).toString();
        try {
            await storeTicketFnumber(
                redisClient,
                ticketId.toString(),
                fibonacci
            );
        } catch (e: any) {
            await handleError(e.toString());
            return;
        }
        await storeFindexFnumber(redisClient, fIndex.toString(), fibonacci);
    } else {
        try {
            await storeTicketFnumber(
                redisClient,
                ticketId.toString(),
                fibonacci
            );
        } catch (e: any) {
            await handleError(e.message);
            return;
        }
    }
    return;
};

process.on('message', async (message: string) => {
    const messageObject = JSON.parse(message) || '';

    if (messageObject.action === 'REDIS_INIT') {
        redisClient = createClient({ url: process.env.REDIS_URL });
        redisClient.on('error', (err: any) => {
            console.log('Redis Client Error (child process)', err);
        });
        redisClient.on('connect', () => {
            console.log('Redis DB is connected (child process)');
        });
        redisClient.connect();
    }

    if (messageObject.action === 'POST_TICKET_PROCESSING') {
        await postTicketProcessing(messageObject); // it sends string in case of error
        // @ts-ignore
        process.send(''); // no error, send empty string to the parent
    }
});

// // Keep it for debug, just to see if the child process is callable
// console.log('mapupab');
// process.on('message', (m) => {
//     console.log('m:', m);
// });
