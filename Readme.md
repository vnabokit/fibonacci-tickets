# App with high RPS value

## Description

This app has two endpoints.

The `/input` endpoint for POST method with a body `{"number":<number>}`. Return a sequence number of the request (named _ticket_). In parallel, the app starts to calculate Fibonacci number for the given `<number>`. When calculated then the app saves the Fibonacci number to a database.

The `/output/:ticket` endpoint for GET method. Return the Fibonacci number for the respective ticket.

## Example

-   Request 1: `curl -X POST -d "{\"number\":6}" http://localhost:5000/input`
-   Response 1: `{ "ticket": 1}`
-   Request 2: `curl -X POST -d "{\"number\":9}" http://localhost:5000/input`
-   Response 2: `{ "ticket": 2}`
-   Request 3: `curl -X GET http://localhost:5000/output/2`
-   Response 3: `{"Fibonacci":"34"}`

## Specific

This app is designed to achieve high request per seconds (RPS) value. Ideally, it should be somewhere near 20k RPS. But I reached only ≈4000 RPS for the both endpoints.

## Tools and technologies

-   Vanilla Node.js `http` module
-   Node.js `child_process` usage
-   Redis DB
-   Docker, docker-compose
-   TypeScript

## Installation and launching

-   Clone the repository
-   Go into the project's folder
-   Rename `.env-example` to `.env`
-   Launch the `docker-compose up --build` command
-   If you see logs that server is run and DB is connected, then the project is ready to work.

## Tests

-   Install the _autocannon_ app: `npm i -g autocannon`
-   Test 1: `autocannon -m POST -b "{\"number\":3}" http://localhost:5000/input`
-   Test 2: `autocannon -m GET http://localhost:5000/output/1`
