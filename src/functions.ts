import { IncomingMessage } from 'http';
// import { RedisClientType } from "redis";

export const getHttpBody = async (req: IncomingMessage): Promise<string> => {
    const buffers = [];
    for await (const chunk of req) {
        buffers.push(chunk);
    }
    return Buffer.concat(buffers).toString();
};

export const calculateValue = (fIndex: number) => {
    if (fIndex < 1) {
        return 0;
    }
    let a = 0;
    let b = 1;
    for (let i = 1; i < fIndex; ++i) {
        const c = a + b;
        a = b;
        b = c;
    }
    return b;
};

export const handleError = async (message: string) => {
    // @ts-ignore
    process.send(message);
};

export const storeFindexFnumber = async (
    redisClient: any,
    fIndex: string,
    fibonacci: string
): Promise<void> => {
    try {
        await redisClient.set('findex:' + fIndex, fibonacci);
        // console.log(
        //     'Saved to DB new (index, number) ' + fIndex + ', ' + fibonacci
        // );
    } catch (e) {
        console.warn('Calculation was not cached.', e);
    }
};

export const storeTicketFnumber = async (
    redisClient: any,
    ticketId: string,
    fibonacci: string
): Promise<void> => {
    try {
        await redisClient.set('ticket:' + ticketId, fibonacci);
        // console.log(
        //     'Saved to DB (ticket, number) ' + ticketId + ', ' + fibonacci
        // );
    } catch (e) {
        throw e;
    }
};

export const getByTicketId = async (
    redisClient: any,
    ticketId: string
): Promise<string> => {
    try {
        // console.log('Retrieve info about ticket: ' + ticketId);
        const result = await redisClient.get('ticket:' + ticketId);
        return result ? result : '';
    } catch (e) {
        throw e;
    }
};

export const getByFindex = async (
    redisClient: any,
    fIndex: string
): Promise<string> => {
    try {
        const result = await redisClient.get('findex:' + fIndex);
        return result ? result : '';
    } catch (e) {
        throw e;
    }
};
