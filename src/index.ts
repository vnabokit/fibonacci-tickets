import 'dotenv/config';
import { createServer, IncomingMessage, ServerResponse } from 'http';
import { fork } from 'child_process';
import { createClient } from 'redis';
import { getHttpBody, getByTicketId } from './functions';

const redisClient = createClient({ url: process.env.REDIS_URL });
redisClient.on('error', (err: any) => {
    console.log('Redis Client Error (parent)', err);
});
redisClient.on('connect', () => {
    console.log('Redis DB is connected (parent)');
});
redisClient.connect();

const child = fork(__dirname + '/child_process_jobs');
child.send(
    JSON.stringify({
        action: 'REDIS_INIT',
    })
);
// // Keep it for debug, just to see if the child process is callable
// child.send('testm');

const PORT = 5000;
const ENDPOINT1 = { path: '/input', method: 'POST' };
const ENDPOINT2 = { path: '/output', method: 'GET' };

let ticketId = 0;

const server = createServer(
    async (req: IncomingMessage, resp: ServerResponse) => {
        resp.setHeader('Content-Type', 'text/html');
        const urlString: string = req.url || '';

        if (urlString === ENDPOINT1.path && req.method === ENDPOINT1.method) {
            ticketId = ticketId + 1;
            resp.statusCode = 200;
            resp.write('{ "ticket": ' + ticketId + '}');
            resp.end();

            // // If uncomment then RpS will decrease for 1k..2k rps
            // // But we will have a feedback from the child process
            // // and may send messages about errors if any.
            // // For now, these messages will not be sent
            // // because resp.end() has been already executed.
            // // Some another approache should be applied.
            // child.on("message", async (message: string) => {
            //   if (message.length > 0) {
            //     resp.write("\n" + message);
            //   } else {
            //     resp.end();
            //   }
            // });
            child.send(
                JSON.stringify({
                    action: 'POST_TICKET_PROCESSING',
                    body: await getHttpBody(req),
                    ticketId,
                })
            );
        } else if (
            urlString.indexOf(ENDPOINT2.path) === 0 &&
            req.method === ENDPOINT2.method
        ) {
            const givenTicketId = urlString.substring(
                ENDPOINT2.path.length + 1
            );
            const fibonacci: string = await getByTicketId(
                redisClient,
                givenTicketId
            );
            let respObject = '';
            if (fibonacci !== '') {
                respObject = JSON.stringify({ Fibonacci: fibonacci });
                resp.statusCode = 200;
            } else {
                resp.statusCode = 404;
                respObject = 'Not found';
            }
            resp.write(respObject);
            resp.end();
        } else {
            resp.statusCode = 404;
            resp.end('No such endpoint');
        }
    }
);

server.listen(PORT, () => {
    console.log(`Server is started at ${PORT}`);
});
