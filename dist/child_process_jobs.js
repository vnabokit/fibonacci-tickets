"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const redis_1 = require("redis");
const functions_1 = require("./functions");
let redisClient;
const postTicketProcessing = async (messageObject) => {
    const bodyObject = JSON.parse(messageObject.body);
    const fIndex = parseInt(bodyObject.number, 10);
    if (fIndex < 1) {
        await (0, functions_1.handleError)('Number should be > 0');
        return;
    }
    let fibonacci = await (0, functions_1.getByFindex)(redisClient, fIndex.toString());
    const ticketId = messageObject.ticketId;
    if (fibonacci === '') {
        fibonacci = (0, functions_1.calculateValue)(fIndex).toString();
        try {
            await (0, functions_1.storeTicketFnumber)(redisClient, ticketId.toString(), fibonacci);
        }
        catch (e) {
            await (0, functions_1.handleError)(e.toString());
            return;
        }
        await (0, functions_1.storeFindexFnumber)(redisClient, fIndex.toString(), fibonacci);
    }
    else {
        try {
            await (0, functions_1.storeTicketFnumber)(redisClient, ticketId.toString(), fibonacci);
        }
        catch (e) {
            await (0, functions_1.handleError)(e.message);
            return;
        }
    }
    return;
};
process.on('message', async (message) => {
    const messageObject = JSON.parse(message) || '';
    if (messageObject.action === 'REDIS_INIT') {
        redisClient = (0, redis_1.createClient)({ url: process.env.REDIS_URL });
        redisClient.on('error', (err) => {
            console.log('Redis Client Error (child process)', err);
        });
        redisClient.on('connect', () => {
            console.log('Redis DB is connected (child process)');
        });
        redisClient.connect();
    }
    if (messageObject.action === 'POST_TICKET_PROCESSING') {
        await postTicketProcessing(messageObject); // it sends string in case of error
        // @ts-ignore
        process.send(''); // no error, send empty string to the parent
    }
});
// // Keep it for debug, just to see if the child process is callable
// console.log('mapupab');
// process.on('message', (m) => {
//     console.log('m:', m);
// });
//# sourceMappingURL=child_process_jobs.js.map