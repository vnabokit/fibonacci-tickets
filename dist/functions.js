"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getByFindex = exports.getByTicketId = exports.storeTicketFnumber = exports.storeFindexFnumber = exports.handleError = exports.calculateValue = exports.getHttpBody = void 0;
// import { RedisClientType } from "redis";
const getHttpBody = async (req) => {
    const buffers = [];
    for await (const chunk of req) {
        buffers.push(chunk);
    }
    return Buffer.concat(buffers).toString();
};
exports.getHttpBody = getHttpBody;
const calculateValue = (fIndex) => {
    if (fIndex < 1) {
        return 0;
    }
    let a = 0;
    let b = 1;
    for (let i = 1; i < fIndex; ++i) {
        const c = a + b;
        a = b;
        b = c;
    }
    return b;
};
exports.calculateValue = calculateValue;
const handleError = async (message) => {
    // @ts-ignore
    process.send(message);
};
exports.handleError = handleError;
const storeFindexFnumber = async (redisClient, fIndex, fibonacci) => {
    try {
        await redisClient.set('findex:' + fIndex, fibonacci);
        // console.log(
        //     'Saved to DB new (index, number) ' + fIndex + ', ' + fibonacci
        // );
    }
    catch (e) {
        console.warn('Calculation was not cached.', e);
    }
};
exports.storeFindexFnumber = storeFindexFnumber;
const storeTicketFnumber = async (redisClient, ticketId, fibonacci) => {
    try {
        await redisClient.set('ticket:' + ticketId, fibonacci);
        // console.log(
        //     'Saved to DB (ticket, number) ' + ticketId + ', ' + fibonacci
        // );
    }
    catch (e) {
        throw e;
    }
};
exports.storeTicketFnumber = storeTicketFnumber;
const getByTicketId = async (redisClient, ticketId) => {
    try {
        // console.log('Retrieve info about ticket: ' + ticketId);
        const result = await redisClient.get('ticket:' + ticketId);
        return result ? result : '';
    }
    catch (e) {
        throw e;
    }
};
exports.getByTicketId = getByTicketId;
const getByFindex = async (redisClient, fIndex) => {
    try {
        const result = await redisClient.get('findex:' + fIndex);
        return result ? result : '';
    }
    catch (e) {
        throw e;
    }
};
exports.getByFindex = getByFindex;
//# sourceMappingURL=functions.js.map